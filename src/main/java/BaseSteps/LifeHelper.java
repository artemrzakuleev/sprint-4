package BaseSteps;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;


public class LifeHelper {
    public WebDriver driver;
    public String xPath;
    public Duration duration=Duration.ofSeconds(20);
    public String catalogBlock="//div[@class='catalog-block ember-view catalog__block catalog__search-results']";
    public String resetSearch="//button[@class='st-button_style_none search-form__reset']";
    public LifeHelper(WebDriver driver) {
        this.driver=driver;
    }
    @Step("Click element '{element}'")
    public void clickElement(String element)    {
        findElement(element).click();
    }
    @Step("Ввести в поле '{field}' значение '{value}'")
    public void enterValue(String element, String value)    {
        findElement(element).sendKeys(value);
    }
    @Step("Open Main Page")
    public void openMainPage()  {
        driver.get("https://stepik.org/catalog");
    }
    @Step
    public WebElement findElement(String element){
        WebDriverWait webDriverWait=new WebDriverWait(driver,duration);
        webDriverWait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(element))));
        return driver.findElement(By.xpath(element));
    }
}